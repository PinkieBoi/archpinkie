#!/usr/bin/env bash
#
#   ______ _       _    _     ______       _
#   | ___ (_)     | |  (_)    | ___ \     (_)
#   | |_/ /_ _ __ | | ___  ___| |_/ / ___  _
#   |  __/| | '_ \| |/ / |/ _ \ ___ \/ _ \| |
#   | |   | | | | |   <| |  __/ |_/ / (_) | |
#   \_|   |_|_| |_|_|\_\_|\___\____/ \___/|_|
#   -----------------------------------------

function config {
   /usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $@
}

echo "+--------------------------------------------+"
echo "|  Pulling down personal config from GitLab  |"
echo "+--------------------------------------------+"
 
git clone git@github.com:PinkieBoi/nvim.git $HOME/.config/nvim
git clone --bare git@gitlab.com:PinkieBoi/dotfiles.git $HOME/.dotfiles
git clone git@gitlab.com:PinkieBoi/wallpapers.git $HOME/Pictures/Wallpapers

config checkout --force
config config --local status.showUntrackedFiles no

echo ""
echo "Done!"
echo ""
