#!/usr/bin/env bash

### GPU specific software
RADEON=("ttf-liberation" "ttf-ms-win11-auto" "mesa" "lib32-mesa" "xf86-video-amdgpu" "libva-mesa-driver" "lib32-libva-mesa-driver" "mesa-vdpau" "lib32-mesa-vdpau" "opencl-amd")

NVIDIA=('nvidia-dkms' 'nvidia-utils' 'nvidia-settings' 'opencl-nvidia' 'cuda' 'cuda-tools'
    'libvdpau' 'libxnvctrl' 'optimus-manager' 'optimus-manager-qt' 'bbswitch-dkms'
    'acpi_call-dkms'
)

if [ "$1" == "AMD" ] || [ "$1" == "amd" ]; then
        echo " "
        echo "#########################################"
        echo "###         Installing AMD            ###"
        echo "#########################################"
        echo " "
        for PKG in "${RADEON[@]}"; do
            echo "Installing: ${PKG}"
            "$2" -S "$PKG" --noconfirm --needed
        done
elif [ "$1" == "nvidia" ]; then
        echo " "
        echo "#########################################"
        echo "###         Installing nvidia         ###"
        echo "#########################################"
        echo " "
        for PKG in "${NVIDIA[@]}"; do
            echo "Installing: ${PKG}"
            "$2" -S "$PKG" --noconfirm --needed
        done
        echo " "
        echo "#########################################"
        echo "###      Enabling Optimus Manager     ###"
        echo "#########################################"
        echo " "
        sudo systemctl enable optimus-manager.service
else
    printf "No GPU or GPU not recognised.\n Consult the Arch Wiki for help"
fi

