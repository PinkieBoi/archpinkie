#!/bin/bash
#   ______ _       _    _     ______       _ 
#   | ___ (_)     | |  (_)    | ___ \     (_)
#   | |_/ /_ _ __ | | ___  ___| |_/ / ___  _ 
#   |  __/| | '_ \| |/ / |/ _ \ ___ \/ _ \| |
#   | |   | | | | |   <| |  __/ |_/ / (_) | |
#   \_|   |_|_| |_|_|\_\_|\___\____/ \___/|_|
#   -----------------------------------------

### Establish global defaults for Git ###

# Tutorials can be found at:
# https://www.atlassian.com/git/tutorials

echo ""
echo "Setting global git config"
echo ""

git config --global core.editor nvim
git config --global pull.rebase false
git config --global push.default simple
git config --global init.defaultBranch master
git config --global user.name "J MacPherson"
git config --global user.email "jtdmacpherson@pinkieboi.com"
git config --global credential.helper 'cache --timeout=32000'
