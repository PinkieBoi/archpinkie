#!/usr/bin/env bash
#set -e
#
#   ______ _       _    _     ______       _
#   | ___ (_)     | |  (_)    | ___ \     (_)
#   | |_/ /_ _ __ | | ___  ___| |_/ / ___  _
#   |  __/| | '_ \| |/ / |/ _ \ ___ \/ _ \| |
#   | |   | | | | |   <| |  __/ |_/ / (_) | |
#   \_|   |_|_| |_|_|\_\_|\___\____/ \___/|_|
#   -----------------------------------------

DESKTOP=(
    #---Linux--------------------------------
    'linux-zen'
    'linux-zen-headers'
    'mkinitcpio-firmware'
    'man-db'
    'man-pages'
    'texinfo'
    'make'
    'apparmor'
    #---Boot---------------------------------
    'efibootmgr'
    'grub-btrfs'
    'os-prober-btrfs'
    #---Xorg---------------------------------
    'mesa'
    'xorg-xhost'
    'xdg-user-dirs'
    'xclip'
    #---Wayland------------------------------
    'wayland'
    'qt5-wayland'
    'qt6-wayland'
    'wlroots'
    'wl-clipboard'
    #---Basic-Desktop------------------------
    'alacritty'
    'base-devel'
    'bash-completion'
    'npm'
    'hwinfo'
    'awesome'
    'awesome-freedesktop-git'
    'vicious'
    'qtile'
    'qtile-extras'
    'kitty'
    'sxhkd'
    'rofi'
    'python-psutil'
    'xcb-util-cursor'
    'variety'
    'picom'
    'openssh'
    'ntp'
    'zsh'
    'zsh-autosuggestions'
    'zsh-syntax-highlighting'
    'starship'
    'fzf'
    #---Storage-Management-------------------
    'gnome-disk-utility'
    #---Python-------------------------------
    'python-wheel'
    'python-setuptools'
    'python-pipenv'
    #---Neovim-------------------------------
    'neovim'
    'python-pynvim'
    'nodejs-neovim'
    'ruby-neovim'
    'neovim-symlinks'
    'ripgrep'
    'fd'
    'luarocks'
    #---Audio--------------------------------
    'pipewire'
    'wireplumber'
    'pulseaudio'
    'pulseaudio-alsa'
    'pulseaudio-bluetooth'
    'pavucontrol'
    'alsa-firmware'
    'alsa-lib'
    'alsa-utils'
    'alsa-plugins'
    'gstreamer'
    'gst-libav'
    'gst-plugins-good'
    'gst-plugins-bad'
    'gst-plugins-base'
    'gst-plugins-ugly'
    'volumeicon'
    #---Internet-----------------------------
    'dhclient'
    'dialog'
    'libsecret'
    'networkmanager'
    'networkmanager-openvpn'
    'network-manager-applet'
    'openvpn'
    'mullvad-vpn'
    'wpa_supplicant'
    #---Bluetooth----------------------------
    'bluez'
    'bluez-utils'
    'bluez-libs'
    'blueberry'
    #---Theming-Utilities--------------------
    'dconf-editor'
    'imagemagick'
    'ocs-url'
    'picom'
    'qt5-styleplugins'
    'qt6ct-kde'
    'variety'
    #---Theming------------------------------
    'arc-gtk-theme'
    'arc-icon-theme'
    'breeze-icons'
    'kvantum-theme-materia'
    'moka-icon-theme-git'
    'materia-gtk-theme'
    'numix-icon-theme-git'
    'papirus-icon-theme'
    'xcursor-breeze'
    'lolcat'
    'shell-color-scripts-git'
    #---Fonts--------------------------------
    'nerd-fonts'
    'adobe-source-sans-pro-fonts'
    'awesome-terminal-fonts'
    'cantarell-fonts'
    'inter-font'
    'noto-fonts'
    'tamsyn-font'
    'terminus-font'
    'ttf-bitstream-vera'
    'ttf-dejavu'
    'ttf-droid'
    'ttf-hack'
    'ttf-inconsolata'
    'ttf-liberation'
    'ttf-ms-fonts'
    'ttf-roboto'
    'ttf-ubuntu-font-family'
    #---Laptop stuff-------------------------
    # 'powertop'
    # 'tlp'
    # 'xorg-xbacklight'

)

# Install software
for PKG in "${DESKTOP[@]}"; do
    echo "Installing: ${PKG}"
    "$1" -S "$PKG" --noconfirm
done

# Enable services after installation
echo " "
echo "#########################################"
echo "###         Enabling  Services        ###"
echo "#########################################"
echo " "
xdg-user-dirs-update --force
sudo systemctl enable NetworkManager
sudo systemctl enable bluetooth.service
# sudo systemctl enable tlp.service
sudo systemctl enable ntpd.service
sudo systemctl enable sshd.service
