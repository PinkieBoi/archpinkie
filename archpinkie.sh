#!/usr/bin/env bash
#
#   ______ _       _    _     ______       _
#   | ___ (_)     | |  (_)    | ___ \     (_)
#   | |_/ /_ _ __ | | ___  ___| |_/ / ___  _
#   |  __/| | '_ \| |/ / |/ _ \ ___ \/ _ \| |
#   | |   | | | | |   <| |  __/ |_/ / (_) | |
#   \_|   |_|_| |_|_|\_\_|\___\____/ \___/|_|
#   -----------------------------------------

gpu="$1"
source ./aur-helper.sh
./git-config.sh
./gpu.sh "$1" $helper
./1-linux-desktop.sh $helper
./2-extra-software.sh $helper

./config.sh


echo ""
echo "All Done! - Reboot Now."
echo ""

