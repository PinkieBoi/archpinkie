#!/usr/bin/env bash
#set -e
#
#   ______ _       _    _     ______       _
#   | ___ (_)     | |  (_)    | ___ \     (_)
#   | |_/ /_ _ __ | | ___  ___| |_/ / ___  _
#   |  __/| | '_ \| |/ / |/ _ \ ___ \/ _ \| |
#   | |   | | | | |   <| |  __/ |_/ / (_) | |
#   \_|   |_|_| |_|_|\_\_|\___\____/ \___/|_|
#   -----------------------------------------

sudo pacman -S git

# Install AUR helper if necessary
if pacman -Qi paru > /dev/null ; then
    echo " "
    echo "##########################################"
    echo "###       paru already inatalled       ###"
    echo "##########################################"
    echo " "
    helper="paru"
elif
    pacman -Qi yay > /dev/null ; then
    echo " "
    echo "##########################################"
    echo "###       paru already inatalled       ###"
    echo "##########################################"
    echo " "
    helper="yay"
else
    echo " "
    echo "##########################################"
    echo "###  Installing the 'paru' aur helper  ###"
    echo "##########################################"
    echo " "
    git clone https://aur.archlinux.org/paru.git
    (cd paru || exit && makepkg -si)
    rm -rf paru
    helper="paru"
fi

