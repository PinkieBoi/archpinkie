#!/usr/bin/env bash
#set -e
#   ______ _       _    _     ______       _
#   | ___ (_)     | |  (_)    | ___ \     (_)
#   | |_/ /_ _ __ | | ___  ___| |_/ / ___  _
#   |  __/| | '_ \| |/ / |/ _ \ ___ \/ _ \| |
#   | |   | | | | |   <| |  __/ |_/ / (_) | |
#   \_|   |_|_| |_|_|\_\_|\___\____/ \___/|_|
#   -----------------------------------------

echo " "
echo "########################################"
echo "##   Installing additional software   ##"
echo "########################################"
echo " "


PKGS=(
    'curl'
    'exa'
    'lsd'
    'tealdeer'
    'bat'
    'bash-zsh-insulter'
    'topgrade-bin'
    'file-roller'
    'gufw'
    'hardinfo'
    'btop'
    'neofetch'
    'rclone'
    'rsync'
    'unrar'
    'unzip'
    'wget'
    'yt-dlp'
    'starship'
    'zenity'
    'zip'
    'auto-cpufreq'
    'autofs'
    'bleachbit'
    'exfatprogs'
    'ntfs-3g'
    'timeshift'
    'k3b'
    'rlwrap'
    'dex'
    'gnome-keyring'
    'libgnome-keyring'
    'bitwarden'
    'libreoffice-fresh'
    'hunspell'
    'hunspell-en_gb'
    'hunspell-en_us'
    'hyphen'
    'hyphen-en'
    'intellij-idea-community-edition'
    'kate'
    'kdeconnect'
    'meld'
    'libmythes'
    'mythes-en'
    'pycharm-community-edition'
    'okular'
    #---Theming------------------------------
    'catppuccin-cursors-macchiato'
    'catppuccin-cursors-mocha'
    'catppuccin-gtk-theme-macchiato'
    'catppuccin-gtk-theme-mocha'
    'catppuccin-macchiato-grub-theme-git'
    'catppuccin-mocha-grub-theme-git'
    'catppuccin-sddm-theme-macchiato'
    'catppuccin-sddm-theme-mocha'
    'papirus-folders-catppuccin-git'
    'plymouth-theme-catppuccin-macchiato-git'
    'plymouth-theme-catppuccin-mocha-git'
    #---Internet-----------------------------
    'brave-bin'
    'qbittorrent'
    'zen-browser-bin'
    #---Communiction-------------------------
    'discord'
    'hexchat'
    'signal-desktop'
    'telegram-desktop'
    #---Virtualization-----------------------
    'virtualbox'
    'virtualbox-sdk'
    'virtualbox-host-dkms'
    'virtualbox-guest-utils'
    'virtualbox-guest-iso'
    'libvirt'
    'ebtables'
    'virt-manager'
    'virt-viewer'
    'dnsmasq'
    'lvm2'
    'openbsd-netcat'
    'qemu-desktop'
    'qemu-emulators-full'
    'swtpm'
    #---Package-Managment--------------------
    'paru'
    'pamac-flatpak'
    'pamac-tray-icon-plasma'
    'yay'
    #---Video--------------------------------
    'flameshot'
    'simplescreenrecorder'
    'vlc'
    #---Compatibility------------------------
    'wine'
    'wine-mono'
    'wine-gecko'
    'dxvk-bin'
    'vkd3d'
    #---Gaming-------------------------------
    'steam'
    'proton-ge-custom-bin'
    #---Optional-Dependencies----------------
    'kdegraphics-mobipocket'
    'unarchiver'
    'coin-or-mp'
    'mariadb-libs'
    'postgresql-libs'
    'unixodbc'
    'libwpg'
    'pstoedit'
)

yay -R iptables
for PKG in "${PKGS[@]}"; do
    echo "Installing: ${PKG}"
    "$1" -S "$PKG" --noconfirm
done

echo " "
echo "########################################"
echo "##       Enabling auto-cpufreq        ##"
echo "########################################"
echo " "
sudo systemctl enable --now auto-cpufreq.service

echo " "
echo "########################################"
echo "##               Done                 ##"
echo "########################################"
echo " "
